const countries = require('./src/config/countries');
const langToURI = lang => lang.replace('_', '-').toLowerCase();

module.exports = {
  exportPathMap: async function (defaultPathMap) {
    let pathBuilder = {
      '/': { page: '/' },
      ...defaultPathMap
    };
    for (let countryCode in countries) {
      let languages = countries[countryCode];

      for (let i = 0; i < languages.length; i++) {
        let lang = languages[i];

        pathBuilder[`/${countryCode}/${langToURI(lang)}`] = {
          page: '/[code]/[lang]',
          query: { code: countryCode, lang: langToURI(lang) }
        };
      }
    }

    return pathBuilder;
  }
};
