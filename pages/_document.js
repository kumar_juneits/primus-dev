import Document, { Head, Html, Main, NextScript } from 'next/document';
import React from 'react';

import { Constants } from '../src/config/constants';
import { uriToLang } from '../src/config/utils';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    let { lang } = this.props.__NEXT_DATA__.query;

    lang = uriToLang(lang || Constants.defaultCountry.lang);
    return (
      <Html lang={lang}>
        <Head></Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
