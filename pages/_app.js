import 'bootstrap/dist/css/bootstrap.min.css';
import '../src/styles/globals.css';

import * as _ from 'lodash';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';

import { Constants } from '../src/config/constants';
import { uriToLang } from '../src/config/utils';

function MyApp({ Component, pageProps }) {
  const { asPath } = useRouter();
  let { lang } = pageProps;
  lang = uriToLang(lang || Constants.defaultCountry.lang);
  const pageInfo = _.get(pageProps, 'page', '');
  let title = 'Fjällräven Experience';
  if (!_.isEmpty(pageInfo)) {
    const pageTitleData = _.get(pageInfo, 'title');
    title = !_.isEmpty(pageTitleData) ? pageTitleData[lang] : title;
  } else {
    const pageUrl = asPath;
    const pageParam = pageUrl.split('/');
    const paramArray = pageParam[pageParam.length - 1];
    const pageTitle = !_.isEmpty(paramArray) ? paramArray.split('?')[0] : '';
    title = `${pageTitle} | Fjällräven`;
  }
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <title>{title}</title>
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/assets/images/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/assets/images/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/assets/images/favicon-16x16.png"
        />
        <link
          rel="mask-icon"
          href="/assets/images/safari-pinned-tab.svg"
          color="#ca0d36"
        />
        <link rel="shortcut icon" href="/assets/images/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#000000" />
        <meta name="description" content="" />
        <link rel="stylesheet" href="/assets/fonts.css" />
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          crossOrigin="anonymous"
        />
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
