import groq from 'groq';
import * as _ from 'lodash';
import { useRouter } from 'next/router';
import React from 'react';

import CommonHeaderSection from '../src/components/home/commonHeaderSection';
import CommunityUpdatesSection from '../src/components/home/communityUpdatesSection';
import FeaturedEventSection from '../src/components/home/featuredEventSection';
import GetUpdateSection from '../src/components/home/getUpdateSection';
import MoreFromFjallravenSection from '../src/components/home/moreFromFjallravenSection';
import NowLaunchingSection from '../src/components/home/nowLaunchingSection';
import PeopleSection from '../src/components/home/peopleSection';
import RecentVideosSection from '../src/components/home/recentVideosSection';
import TopSelectionSection from '../src/components/home/topSelectionSection';
import TrekkInSwedenSection from '../src/components/home/trekkInSwedenSection';
import WelcomeSection from '../src/components/home/welcomeSection';
import Layout from '../src/components/layout';
import { Constants } from '../src/config/constants';
import sanityClientHandle from '../src/config/sanityClient';
import { uriToLang } from '../src/config/utils';
import { getMenuItems } from '../src/services/sanity';

const HomeIndex = ({ page, countries, menuItems, siteSettings }) => {
  const router = useRouter();
  const lang = uriToLang(router.query.lang || Constants.defaultCountry.lang);
  const content = page && page.content ? page.content[lang] : null;
  const countryCode = router.query.code || Constants.defaultCountry.code;

  let productSectionRendered = false;

  const heroData = _.find(content, {
    _type: Constants.homepageSectionTypes.HERO
  });

  const productData = _.filter(content, {
    _type: Constants.homepageSectionTypes.PRODUCT
  });

  const learnMoreData = _.find(content, {
    _type: Constants.homepageSectionTypes.LEARN_MORE
  });

  const renderComponent = data => {
    if (data.disabled) return;
    if (data._type === Constants.homepageSectionTypes.HERO) return;
    if (data._type === Constants.homepageSectionTypes.VIDEO_SECTION)
      return <RecentVideosSection key={'recentVideos'} data={data} />;
    if (data._type === Constants.homepageSectionTypes.GET_UPDATE_SECTION)
      return (
        <GetUpdateSection
          key={'getUpdate'}
          data={data}
          countryCode={countryCode}
        />
      );
    if (data._type === Constants.homepageSectionTypes.PROMOTION)
      return <NowLaunchingSection key={'nowLaunching'} data={data} />;
    if (data._type === Constants.homepageSectionTypes.TOP_SELECTION_SECTION)
      return <TopSelectionSection key={'topSelection'} data={data} />;

    if (
      (data._type === Constants.homepageSectionTypes.LEARN_MORE ||
        data._type === Constants.homepageSectionTypes.PRODUCT) &&
      !productSectionRendered
    ) {
      if (
        (productData && productData.length > 0) ||
        (learnMoreData && !learnMoreData.disabled)
      ) {
        productSectionRendered = true;
        return (
          <TrekkInSwedenSection
            key={'trekkIn'}
            learnMore={learnMoreData}
            products={productData}
          />
        );
      }
    }

    switch (data.section) {
      case 'feature_event':
        return <FeaturedEventSection key={'featureEvents'} data={data} />;
      case 'people':
        return <PeopleSection key={'people'} data={data} />;
      case 'communityUpdate':
        return <CommunityUpdatesSection key={'communityUpdate'} data={data} />;
      case 'moreFromFj':
        return <MoreFromFjallravenSection key={'moreFromFj'} data={data} />;
      case 'promotion':
        return <CommonHeaderSection key={'promotion'} data={data} />;
      default:
        return;
    }
  };

  return (
    <Layout menuItems={menuItems} siteSettings={siteSettings} content={content}>
      <div className={'page-home'}>
        {heroData && !heroData.disabled && (
          <WelcomeSection data={heroData} countries={countries} />
        )}
        {content && _.isArray(content) && content.map(c => renderComponent(c))}
      </div>
    </Layout>
  );
};

const query = groq`*[_type == "page" && _id=="frontpage"]`;

HomeIndex.getInitialProps = async function (context) {
  const client = sanityClientHandle(context.query.code);
  const pages = await client.fetch(query);

  const queryCountry = groq`*[_type == "country"]{code, flag, lang, title}`;
  const countriesList = await client.fetch(queryCountry);

  const siteSettingsQuery = groq`*[_type == "siteSettings" && _id=="siteSettings"]`;
  const siteSettings = await client.fetch(siteSettingsQuery);

  const menuItems = await getMenuItems(client);

  return {
    menuItems,
    page: pages && pages.length > 0 ? pages[0] : null,
    countries: countriesList,
    siteSettings:
      siteSettings && siteSettings.length > 0 ? siteSettings[0] : null
  };
};

export default HomeIndex;
