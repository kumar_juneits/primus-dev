export const TagData = [
  {
    id: '1',
    title: 'Fjällräven Classic',
    checked: true
  },
  {
    id: '1',
    title: 'Polar'
  },
  {
    id: '1',
    title: 'Classic events'
  },
  {
    id: '1',
    title: 'Campfire events'
  },
  {
    id: '1',
    title: 'Store events'
  },
  {
    id: '1',
    title: 'Brand stores'
  }
];

export const PeopleTagData = [
  {
    id: '1',
    title: 'Fjällräven Guide',
    checked: true
  },
  {
    id: '1',
    title: 'Ambassador'
  },
  {
    id: '1',
    title: 'Event Alumni'
  }
];
