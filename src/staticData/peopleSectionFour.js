export const Data = [
  {
    imgSrc: '/assets/images/people/img1.svg',
    id: '1',
    eventTitle: 'Vanessa Simpson',
    type: 'Guide',
    countryFlag: '/assets/images/people/flag1.svg',
    country: 'SWEDEN',
    eventDesc: '',
    goToEvent: 'Get to know Vanessa'
  },
  {
    imgSrc: '/assets/images/people/img2.svg',
    id: '2',
    eventTitle: 'Jesse Snyder',
    type: 'polar alumni',
    countryFlag: '/assets/images/people/flag1.svg',
    country: 'SWEDEN',
    eventDesc:
      "Description text snippet from event brite describing this event and what it's all about",
    goToEvent: 'Get to know Vanessa'
  },
  {
    imgSrc: '/assets/images/people/img3.svg',
    id: '3',
    eventTitle: 'Toni Price',
    type: 'Guide',
    countryFlag: '/assets/images/people/flag1.svg',
    country: 'SWEDEN',
    eventDesc:
      'Description text snippet from event brite describing this event and what its all about',
    goToEvent: 'Get to know Vanessa'
  },
  {
    imgSrc: '/assets/images/people/img3.svg',
    id: '4',
    eventTitle: 'Toni Price',
    type: 'Guide',
    countryFlag: '/assets/images/people/flag1.svg',
    country: 'SWEDEN',
    eventDesc:
      'Description text snippet from event brite describing this event and what its all about',
    goToEvent: 'Get to know Vanessa'
  },
  {
    imgSrc: '/assets/images/people/img3.svg',
    id: '5',
    eventTitle: 'Toni Price',
    type: 'Guide',
    countryFlag: '/assets/images/people/flag1.svg',
    country: 'SWEDEN',
    eventDesc:
      'Description text snippet from event brite describing this event and what its all about',
    goToEvent: 'Get to know Vanessa'
  }
];
