export const Data = [
  {
    imgSrc: '/assets/images/featuresSection/img1.svg',
    id: '1',
    eventTitle: 'Event Title',
    eventDesc:
      "Description text snippet from event brite describing this event and what it's all about",
    address: 'Kungsgatan 57 B, Stockholm, 111 22',
    date: '2021/13/21',
    time: '13.00',
    goToEvent: 'Go to event',
    badgeSrc: '/assets/images/polarEvent.svg',
    eventName: 'Polar Event'
  },
  {
    imgSrc: '/assets/images/featuresSection/img2.svg',
    id: '1',
    eventTitle: 'Event title',
    eventDesc:
      "Description text snippet from event brite describing this event and what it's all about",
    address: 'Kungsgatan 57 B, Stockholm, 111 22',
    date: '2021/13/21',
    time: '13.00',
    goToEvent: 'Go to event',
    badgeSrc: '/assets/images/icons/badged6.svg',
    eventName: 'Campfire Event'
  },
  {
    imgSrc: '/assets/images/featuresSection/img3.svg',
    id: '1',
    eventTitle: 'Event title',
    eventDesc:
      'Description text snippet from event brite describing this event and what its all about',
    address: 'Kungsgatan 57 B, Stockholm, 111 22',
    date: '2021/13/21',
    time: '13.00',
    goToEvent: 'Go to event',
    badgeSrc: '/assets/images/icons/badged4.svg',
    eventName: 'Classic Event'
  },
  {
    imgSrc: '/assets/images/featuresSection/img3.svg',
    id: '1',
    eventTitle: 'Event title',
    eventDesc:
      "Description text snippet from event brite describing this event and what it's all about",
    address: 'Kungsgatan 57 B, Stockholm, 111 22',
    date: '2021/13/21',
    time: '13.00',
    goToEvent: 'Go to event',
    badgeSrc: '/assets/images/icons/badged6.svg',
    eventName: 'Campfire Event'
  },
  {
    imgSrc: '/assets/images/featuresSection/img3.svg',
    id: '1',
    eventTitle: 'Event title',
    eventDesc:
      "Description text snippet from event brite describing this event and what it's all about",
    address: 'Kungsgatan 57 B, Stockholm, 111 22',
    date: '2021/13/21',
    time: '13.00',
    goToEvent: 'Go to event',
    badgeSrc: '/assets/images/icons/badged4.svg',
    eventName: 'Classic Event'
  },
  {
    imgSrc: '/assets/images/featuresSection/img3.svg',
    id: '1',
    eventTitle: 'Event title',
    eventDesc:
      "Description text snippet from event brite describing this event and what it's all about",
    address: 'Kungsgatan 57 B, Stockholm, 111 22',
    date: '2021/13/21',
    time: '13.00',
    goToEvent: 'Go to event',
    badgeSrc: '/assets/images/icons/badged6.svg',
    eventName: 'Campfire Event'
  },
  {
    imgSrc: '/assets/images/featuresSection/img3.svg',
    id: '1',
    eventTitle: 'Event title',
    eventDesc:
      "Description text snippet from event brite describing this event and what it's all about",
    address: 'Kungsgatan 57 B, Stockholm, 111 22',
    date: '2021/13/21',
    time: '13.00',
    goToEvent: 'Go to event',
    badgeSrc: '/assets/images/icons/badged3.svg',
    eventName: 'Campfire Event'
  }
];
