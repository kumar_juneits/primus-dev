export const CheckData = [
  {
    id: '1',
    title: 'Checkpoint 1'
  },
  {
    id: '2',
    title: 'Checkpoint 2'
  },
  {
    id: '3',
    title: 'Checkpoint 3'
  },
  {
    id: '4',
    title: 'Checkpoint 4'
  },
  {
    id: '5',
    title: 'Checkpoint 5'
  },
  {
    id: '6',
    title: 'Checkpoint 6'
  },
  {
    id: '7',
    title: 'Checkpoint 7'
  },
  {
    id: '8',
    title: 'Finish'
  }
];
