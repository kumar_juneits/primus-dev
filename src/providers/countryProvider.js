import axios from 'axios';
import * as _ from 'lodash';
import { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react';
import React from 'react';

import { Constants } from '../config/constants';

const initCountryData = Constants.defaultCountry;

const CountryContext = React.createContext(null);

export const useCountry = () => {
  const state = useContext(CountryContext);
  if (!state) {
    throw new Error('Error using call in context!');
  }
  return state;
};

export const CountryProvider = ({ children, countriesInit }) => {
  const router = useRouter();
  const [currentCountryData, setCurrentCountryData] = useState(initCountryData);
  const [lang, setLang] = useState(Constants.defaultCountry.lang);
  const [countries, setCountries] = useState(countriesInit || []);
  const [locations, setLocations] = useState([]);

  useEffect(() => {
    const defaultCountry = _.find(countries, {
      code: router.query.code || Constants.defaultCountry.code
    });
    if (defaultCountry) {
      setCurrentCountryData(defaultCountry);
      setLang(router.query.lang || defaultCountry.lang);
    }
    fetchLocations().then();
  }, [countries]);

  const genUrl = path => {
    if (path.startsWith('/')) {
      path = path.substring(1);
    }
    return `/${router.query.code || Constants.defaultCountry.code}/${
      router.query.lang || currentCountryData.lang
    }/${path}`;
  };

  const fetchLocations = async () => {
    const res = await axios.get(
      `${Constants.basePath.LOCATION}/orchestration/state/get?countryCode=${
        router.query.code || currentCountryData.code
      }`
    );
    let loc = [];
    if (res && res.data) {
      loc = res.data.data ? res.data.data : [];
      setLocations(loc);
    }
  };

  const providerValue = {
    currentCountryData,
    setCurrentCountryData,
    countries,
    setCountries,
    lang,
    genUrl,
    locations
  };
  return (
    <CountryContext.Provider value={providerValue}>
      {children}
    </CountryContext.Provider>
  );
};
