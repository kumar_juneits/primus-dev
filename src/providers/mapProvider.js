import axios from 'axios';
import * as _ from 'lodash';
import { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react';
import React from 'react';

import { Config } from '../config/config';
import { Constants } from '../config/constants';
import { uriToLang } from '../config/utils';
import { useCountry } from './countryProvider';

const initMapData = {
  type: Constants.mapRenderTypes.STANDARD,
  center: [17.9355893, 66.205898],
  zoom: 8,
  pitch: 0,
  bearing: 0,
  focusEvent: null,
  events: []
};

const MapContext = React.createContext(null);

export const useMap = () => {
  const state = useContext(MapContext);
  if (!state) {
    throw new Error('Error using call in context!');
  }
  return state;
};

export const MapProvider = ({ children }) => {
  const router = useRouter();
  const { lang } = useCountry();
  const [currentMapData, setCurrentMapData] = useState(initMapData);
  const [currentActivePanel, setCurrentActivePanel] = useState(
    Constants.mapRenderEventActive.MULTI
  );
  const [currentMarkers, setCurrentMarkers] = useState([]);
  const [map, setMap] = useState(null);
  const [eventRouteActive, setEventRouteActive] = useState(null);
  useState(null);
  const [activeEventType, setActiveEventType] = useState(null);
  const [renderPathTraceIndex, setRenderPathTraceIndex] = useState(null);
  const [drawnRouteIndexes, setDrawnRouteIndexes] = useState([]);
  const [homeMapFilterItemCounts, setHomeMapFilterItemCounts] = useState(null);

  const setEvents = async (type, term) => {
    let eventCounts = {};
    let events = [];
    let apiRes = null;
    const countryCode = router.query.code || Constants.defaultCountry.code;
    let postData = {
      countryCode
    };

    if (
      !type ||
      (type !== Constants.eventTypes.brandStoreEvent &&
        type !== Constants.eventTypes.people)
    ) {
      if (type) {
        postData['category'] = [type];
      }
      apiRes = await axios.post(
        `${Constants.basePath.EVENT}/orchestration/event/search`,
        postData
      );

      if (
        apiRes &&
        apiRes.data &&
        apiRes.data.data &&
        apiRes.data.data.eventList
      ) {
        apiRes.data.data.eventList.forEach(e => {
          e.eventType = e.category;
          if (eventCounts[e.eventType]) {
            eventCounts[e.eventType] = eventCounts[e.eventType] + 1;
          } else {
            eventCounts[e.eventType] = 1;
          }
          events.push(e);
        });
      }
      if (type) {
        events = _.filter(events, { eventType: type });
      }
    }

    if (!type || type === Constants.eventTypes.brandStoreEvent) {
      postData = {
        request: {
          appkey: Config.bandifyStoreApiKey,
          formdata: {
            order: 'state, country',
            objectname: 'Locator::Store',
            where: {
              country: {
                in: _.upperCase(countryCode + '')
              }
            }
          }
        }
      };
      apiRes = await axios.post(
        `${Constants.basePath.BANDIFY_STORE}?lang=${uriToLang(lang)}`,
        postData
      );
      if (
        apiRes &&
        apiRes.data &&
        apiRes.data.code === 1 &&
        apiRes.data.response &&
        apiRes.data.response.collection
      ) {
        eventCounts[Constants.eventTypes.brandStoreEvent] =
          apiRes.data.response.collection.length;

        apiRes.data.response.collection.forEach(e => {
          e.eventType = Constants.eventTypes.brandStoreEvent;
          events.push(e);
        });
      }
    }
    if (!type || type === Constants.eventTypes.people) {
      postData = {
        countryCode: countryCode
      };
      apiRes = await axios.post(
        `${Constants.basePath.EVENT}/orchestration/people/search`,
        postData
      );
      if (
        apiRes &&
        apiRes.data &&
        apiRes.data.data &&
        apiRes.data.data.peopleList
      ) {
        eventCounts[Constants.eventTypes.people] =
          apiRes.data.data.peopleList.length;

        apiRes.data.data.peopleList.forEach(e => {
          e.eventType = Constants.eventTypes.people;
          events.push(e);
        });
      }
    }

    if (term) {
      events = _.filter(events, e => {
        return e.title.indexOf(term) > -1;
      });
    }
    setActiveEventType(type);
    let eventData = {
      ...initMapData,
      center:
        events && events.length > 0
          ? [events[0].longitude, events[0].latitude]
          : initMapData.center,
      events,
      focusEvent: null
    };
    setCurrentMapData(eventData);

    if (!homeMapFilterItemCounts) {
      setHomeMapFilterItemCounts(eventCounts);
    }
  };

  const getEventDetails = async event => {
    let apiRes = await axios.post(
      `${Constants.basePath.EVENT}/orchestration/event/search`,
      { id: event.id }
    );
    if (apiRes && apiRes.data && apiRes.data.data) {
      event['panelDetails'] =
        apiRes.data.data.eventList && apiRes.data.data.eventList.length > 0
          ? apiRes.data.data.eventList[0]
          : null;
    }
    return event;
  };

  const getEventRoute = async event => {
    let apiRes = await axios.get(
      // `${Constants.basePath.EVENT}/orchestration/event/route`
      `https://owcda.s3.amazonaws.com/fjallraven-route-map/json/fcuk.json`
    );
    if (
      apiRes &&
      apiRes.data &&
      apiRes.data &&
      apiRes.data.features &&
      apiRes.data.features.length > 0
    ) {
      let featureWaypoints = _.find(apiRes.data.features, {
        id: 'waypoints'
      });
      let featureRoute = _.find(apiRes.data.features, { id: 'route' });
      event['wayPoints'] =
        featureWaypoints &&
        featureWaypoints.geometry &&
        featureWaypoints.geometry.coordinates
          ? featureWaypoints.geometry.coordinates
          : [];

      event['route'] =
        featureRoute &&
        featureRoute.geometry &&
        featureRoute.geometry.coordinates
          ? featureRoute.geometry.coordinates
          : [];

      const route = event['route'];
      let routeSplit = [];
      if (
        featureWaypoints &&
        featureWaypoints.properties &&
        featureWaypoints.properties.equiDistantIndexes
      ) {
        let equiDistantIndexes = featureWaypoints.properties.equiDistantIndexes;
        let prevIndex = 0;
        equiDistantIndexes.forEach(disIndex => {
          if (disIndex > 0) {
            routeSplit.push(route.slice(prevIndex, disIndex));
            prevIndex = disIndex + 1;
          }
        });
      }
      event['routeSplit'] = routeSplit;

      setEventRouteActive(event);
    }
  };

  useEffect(() => {
    setEvents().then();
  }, []);

  useEffect(() => {
    if (eventRouteActive) {
      let eventData = {
        ...currentMapData,
        events: [],
        focusEvent: null
      };
      setCurrentMapData(eventData);
      setCurrentActivePanel(Constants.mapRenderEventActive.ROUTE);
    }
  }, [eventRouteActive]);

  const resetMapCanvas = () => {
    if (window) {
      setTimeout(() => {
        window.dispatchEvent(new Event('resize'));
      }, 5);
    }
  };

  const resetMapEvents = () => {
    setEventRouteActive(null);
    setCurrentActivePanel(Constants.mapRenderEventActive.MULTI);
    setEvents().then();
    resetMapCanvas();
  };

  const setSingleEventFocus = event => {
    let eventData = {
      ...currentMapData,
      center: [event.longitude, event.latitude],
      focusEvent: event
    };
    setCurrentMapData(eventData);

    if (
      _.includes(
        [
          Constants.eventTypes.campfireEvent,
          Constants.eventTypes.classicEvent,
          Constants.eventTypes.store,
          Constants.eventTypes.polarEvent,
          Constants.eventTypes.fjallravenClassic
        ],
        event.eventType
      )
    ) {
      getEventDetails(event).then(eventDetails => {
        getEventRoute(eventDetails).then();
      });
    } else if (event.eventType === Constants.eventTypes.brandStoreEvent) {
      setCurrentActivePanel(Constants.mapRenderEventActive.SINGLE);
    } else {
      setCurrentActivePanel(Constants.mapRenderEventActive.SINGLE);
    }
  };

  const setFilterEventByType = type => {
    setEventRouteActive(null);
    setCurrentActivePanel(Constants.mapRenderEventActive.MULTI);
    setEvents(type).then();
    resetMapCanvas();
  };

  const setEventSearch = term => {
    if (term && term.length > 2) {
      setEvents(null, term).then();
    }
  };

  const providerValue = {
    currentMapData,
    setCurrentMapData,
    map,
    setMap,
    setSingleEventFocus,
    currentActivePanel,
    currentMarkers,
    setCurrentMarkers,
    setEventRouteActive,
    eventRouteActive,
    resetMapEvents,
    renderPathTraceIndex,
    setRenderPathTraceIndex,
    setDrawnRouteIndexes,
    drawnRouteIndexes,
    setFilterEventByType,
    activeEventType,
    setEventSearch,
    homeMapFilterItemCounts
  };
  return (
    <MapContext.Provider value={providerValue}>{children}</MapContext.Provider>
  );
};
