import groq from 'groq';

export const getMenuItems = async client => {
  const query = groq`*[_type == "siteNavigations" && !(_id in path("drafts.**"))]`;
  return await client.fetch(query);
};
export const getupdatesModelData = async client => {
  const query = groq`*[_type == "page" && _id=="frontpage"]`;
  return await client.fetch(query);
};

export const getChefList = async client => {
  const query = groq`*[_type == "chef" && !(_id in path("drafts.**"))]`;
  return await client.fetch(query);
};
