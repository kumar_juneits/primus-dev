export const Constants = {
  basePath: {
    EVENT: 'https://app-b2cbep-uat-001.azurewebsites.net',
    PEOPLE: 'https://app-b2cbep-uat-001.azurewebsites.net',
    BLOG: 'https://app-b2cbep-uat-001.azurewebsites.net',
    LOCATION: 'https://app-b2cbep-uat-001.azurewebsites.net',
    BANDIFY_STORE: 'https://api.where2stageit.com/fjallraven/rest/getlist',
    HEADER_DATA: '/epi/',
    EPI_SERVER_TOKEN: 'https://inte.fjallraven.com/episerverapi/token',
    EPI_SERVER_CART_DETAILS:
      'https://inte.fjallraven.com/en-gb/api/bependpoints/userstatus',
    EPI_SERVER_CART_ITEM_UPDATE:
      'https://inte.fjallraven.com/en-gb/api/bependpoints/updatecart',
    SUBSCRIBEUSER:
      'https://www.fjallraven.com/de-de/api/bependpoints/subscribeuser',
    ORCHESTRATION_API_URL:
      'https://app-b2cbep-uat-001.azurewebsites.net/orchestration/state/epiServerEndpoints'
  },
  epiServerToken: {
    grant_type: 'password',
    username: 'bep@june.com',
    password: 'c2c48b57'
  },
  activeUserIdKey: 'activeUserId',
  mapRenderTypes: {
    STANDARD: 'standard'
  },
  defaultCountry: {
    name: 'United Kingdom',
    lang: 'en-gb',
    code: 'uk',
    database: 'uk'
  },
  flowboxCountryMapping: {
    uk: {
      lang: 'en-GB',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    no: {
      lang: 'no-NO',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    de: {
      lang: 'de-De',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    nl: {
      lang: 'nl-Nl',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    fr: {
      lang: 'fr-Fr',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    fi: {
      lang: 'fi-Fi',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    se: {
      lang: 'sv-Se',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    dk: {
      lang: 'da-Dk',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    eu: {
      lang: 'en-EU',
      key: '5qJd3wV6TwerIAL5JBMBMg'
    },
    us: {
      lang: null,
      key: '3LaoewK1RJKQOS-IqvPh0A'
    },
    ca: {
      lang: null,
      key: '3LaoewK1RJKQOS-IqvPh0A'
    }
  },
  homepageHeroEventCodeReplaceText: '_eventCount_',
  mapRenderEventActive: {
    MULTI: 'multi',
    SINGLE: 'single',
    ROUTE: 'route'
  },
  eventTypes: {
    campfireEvent: 'campfireEvent',
    brandStoreEvent: 'brandStoreEvent',
    classicEvent: 'classicEvent',
    polarEvent: 'polarEvent',
    fjallravenClassic: 'fjallravenClassic',
    store: 'store',
    people: 'people'
  },
  peopleFilterTypes: {
    guide: 'guide',
    ambassador: 'ambassador',
    alumni: 'alumni'
  },
  articleFilterTypes: {
    adventure: 'adventure',
    guide: 'guide',
    sustainability: 'sustainability',
    product: 'product'
  },
  eventTypeDisplayName: {
    campfireEvent: 'Campfire Event',
    brandStoreEvent: 'Brand store',
    classicEvent: 'Classic Event',
    polarEvent: 'Polar Event',
    fjallravenClassic: 'Fjällräven Classic',
    store: 'Store Event'
  },
  homepageSectionTypes: {
    HERO: 'hero',
    PRODUCT: 'product',
    LEARN_MORE: 'learnMore',
    GET_UPDATE_SECTION: 'getUpdateSection',
    WELCOME_MESSAGE: 'welcomeMessage',
    PROMOTION: 'promotion',
    TOP_SELECTION_SECTION: 'topSelectionSection',
    VIDEO_SECTION: 'videoSection',
    TOP_SELECTION: 'top_selection',
    MAP: 'map',
    PEOPLE: 'people',
    FEATURE_EVENT: 'feature_event',
    COMMUNITY_UPDATE: 'communityUpdate',
    MORE_FROM_FJ: 'moreFromFj'
  }
};
