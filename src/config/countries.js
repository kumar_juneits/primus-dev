const countries = {
  uk: ['en-gb'],
  de: ['de-de'],
  fr: ['fr-fr']
};
module.exports = countries;
