import moment from 'moment';

export function langToURI(lang) {
  return lang.replace('_', '-').toLowerCase();
}

export function uriToLang(lang) {
  return lang.replace('-', '_').toLowerCase();
}

export function getMonths(nextNumberOfMonths) {
  const month = currentDate => {
    let futureMonthDate = moment(currentDate).add(1, 'M');
    return {
      month: futureMonthDate.month() + 1,
      year: futureMonthDate.year(),
      monthDisplayName: futureMonthDate.format('MMMM'),
      date: futureMonthDate.toDate()
    };
  };

  let output = [];
  let curMonth = new Date();
  for (let i = 0; i < nextNumberOfMonths; i++) {
    let monthDetails = month(curMonth);
    curMonth = monthDetails.date;
    output.push(monthDetails);
  }

  return output;
}

export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function getLangUrl(url) {
  let el = document.createElement('a');
  el.href = url;
  return el.pathname;
}
