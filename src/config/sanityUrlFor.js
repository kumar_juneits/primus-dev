import imageUrlBuilder from '@sanity/image-url';

import sanityClientHandler from './sanityClient';

export function sanityUrlFor(source, code) {
  const client = sanityClientHandler(code);

  const builder = imageUrlBuilder(client);
  return builder.image(source);
}
