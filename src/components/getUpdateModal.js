import * as _ from 'lodash';
import React from 'react';
import { Modal } from 'react-bootstrap';

import { Constants } from '../../src/config/constants';
import { sanityUrlFor } from '../../src/config/sanityUrlFor';
import { useGlobal } from '../providers/globalProvider';
const BlockContent = require('@sanity/block-content-to-react');
import styles from '../styles/home/getUpdateSectionStyle.module.css';

const GetUpdateModal = ({ content }) => {
  const { showGetUpdateModal, setShowGetUpdateModal } = useGlobal();
  const handleClose = () => setShowGetUpdateModal(false);
  const modelData = _.find(content, {
    _type: Constants.homepageSectionTypes.GET_UPDATE_SECTION
  });
  const backgroundImageUrl = _.get(modelData, 'backgroundImage.asset', '');
  const heading = _.get(modelData, 'heading', '');
  const subtitle = _.get(modelData, 'subtitle', '');
  const termText = _.get(modelData, 'termText', '');
  return (
    <Modal
      show={showGetUpdateModal}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      size="lg"
    >
      <a
        href="/"
        className="popClose"
        onClick={e => {
          e.preventDefault();
          handleClose();
        }}
      >
        <img src="/assets/images/close.svg" data-dismiss="modal" alt={''} />
      </a>
      <section className={'pop-section-container backgroudTransparent'}>
        <div className="container p-0">
          <div className={`d-flex  ${styles.card}`}>
            <div
              className={styles.cardImgTop}
              style={{
                backgroundImage: `url(${sanityUrlFor(backgroundImageUrl)})`
              }}
            />
            <div
              className={`text-left pl-3 py-5 d-flex align-items-center justify-content-center pop-left-container ${styles.cardBody}`}
            >
              <div className={`py-5 ${styles.formBox}`}>
                <span className={styles.cardTitle}>{heading}</span>
                <p className={styles.cardText}>{subtitle}</p>
                <div className={styles.emailInput}>
                  <input
                    type="email"
                    className={styles.emailSection}
                    placeholder="Your e-mail"
                  />
                  <img
                    src="/assets/images/sectionThree/RightArrow.svg"
                    alt=""
                    width="8px"
                    height="18px"
                    className={styles.modal_Arrow}
                  />
                </div>
                <div className={styles.terms}>
                  <label className={styles.checkboxDiv}>
                    <BlockContent blocks={termText} />
                    <input type="checkbox" />
                    <span className={styles.checkMark} />
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Modal>
  );
};

export default GetUpdateModal;
