import React, { useState } from 'react';

import { capitalizeFirstLetter, getLangUrl } from '../config/utils';

const TopNavigationWelcome = ({ onClose, data }) => {
  const [selectedCountry, setSelectedCountry] = useState();
  const [showLang, setShowLang] = useState(false);

  const getFirstValue = languageUrlDictionary => {
    let firstKey = Object.keys(languageUrlDictionary)[0];
    return firstKey ? languageUrlDictionary[firstKey] : '';
  };

  const selectedMarket = (e, market) => {
    e.preventDefault();
    setSelectedCountry(market);
    let keys = Object.keys(market.languageUrlDictionary);
    if (keys && keys.length > 1) {
      setShowLang(true);
    } else {
      if (market.hideUrl) {
        window.location = getLangUrl(
          getFirstValue(market.languageUrlDictionary)
        );
      } else {
        window.location = getFirstValue(market.languageUrlDictionary);
      }
    }
  };
  return (
    <div
      className="region--root first-visit region--root-view"
      open
      tabIndex={-1}
    >
      <div className="region--container top-navigation-container-wp">
        <a
          href="javascript:void(0)"
          aria-label="close"
          className="region--close"
          tabIndex={0}
          onClick={() => onClose(false)}
        />
        {!showLang && (
          <div className="js-welcome-area">
            <div className="teaser--root divider--root">
              <div className="divider--divider">
                <img
                  src="/assets/images/save-the-arctic-fox.png"
                  alt=""
                  className="divider--image"
                />
              </div>
            </div>
            <div className="divider-bar-area">
              <span className="divider--subtitle">
                <p>
                  Please select your{' '}
                  <span className="semi-accent">Shipping Region:</span>
                </p>
              </span>
            </div>
          </div>
        )}
        {showLang && (
          <div className="js-lang-area">
            <div className="teaser--root divider--root">
              <div className="divider--divider">
                <img
                  src="/assets/images/save-the-arctic-fox.png"
                  alt=""
                  className="divider--image"
                />
                <div className="divider--content">
                  <span className="divider--title">Select Language</span>
                </div>
              </div>
            </div>
            <span className="region--country selected">
              <span
                className={`flag-icon ${selectedCountry.flag} region--country-flag`}
              />
              <span>
                <span className="region--country-title">
                  {selectedCountry.marketName}
                  <span className="region--country-currency">
                    | {selectedCountry.currencySymbol}
                  </span>
                </span>
              </span>
            </span>
            <span className="divider--subtitle">
              <p>
                Please select your{' '}
                <span className="semi-accent">Preferred Language</span>
              </p>
            </span>
          </div>
        )}
        {showLang && selectedCountry && (
          <div className="region--lang js-lang-area">
            <div data-market={selectedCountry.marketName}>
              <div className="region--lang-item-holder">
                {Object.keys(selectedCountry.languageUrlDictionary).map(
                  (key, index) => (
                    <a
                      key={key}
                      href={getLangUrl(
                        selectedCountry.languageUrlDictionary[key]
                      )}
                      className="region--lang-item"
                      tabIndex={index}
                    >
                      {capitalizeFirstLetter(key)}
                    </a>
                  )
                )}
              </div>
            </div>
          </div>
        )}
        {!showLang && (
          <div className="region--groups js-region-groups">
            {data.regionSelector &&
              data.regionSelector.marketGroups &&
              data.regionSelector.marketGroups.map((group, index) => (
                <div
                  className={'region--groups-column-' + (index + 1)}
                  key={index}
                >
                  <span className="region--group-title">{group.groupName}</span>

                  <nav className="region--countries">
                    {group &&
                      group.availableMarkets &&
                      group.availableMarkets.map((market, inIndex) => (
                        <a
                          key={inIndex}
                          className="region--country "
                          href={getFirstValue(market.languageUrlDictionary)}
                          data-flag={market.flag}
                          data-market={market.marketName}
                          data-currency-symbol={market.currencySymbol}
                          tabIndex={inIndex}
                          onClick={e => selectedMarket(e, market)}
                        >
                          <span
                            className={`flag-icon ${market.flag}  region--country-flag`}
                          />
                          <span>
                            <span className="region--country-title">
                              {market.marketName}
                              <span className="region--country-currency">
                                | {market.currencySymbol}
                              </span>
                            </span>
                            {!market.hideUrl && (
                              <span className="region--country-domain">
                                {getFirstValue(market.languageUrlDictionary)}
                              </span>
                            )}
                          </span>
                        </a>
                      ))}
                  </nav>
                </div>
              ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default TopNavigationWelcome;
