export const ChevronRight = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="12"
    height="12"
    fill="currentColor"
    className="bi bi-chevron-right"
    viewBox="0 0 16 16"
  >
    <path
      fillRule="evenodd"
      d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
    />
  </svg>
);
export const ChevronRightArrow = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M9.41142 21C9.04885 21 8.68629 20.855 8.41318 20.56C7.86227 19.9749 7.86227 19.0249 8.41318 18.4398L14.0071 12.4994L8.41318 6.55894C7.86227 5.9739 7.86227 5.02383 8.41318 4.43878C8.9641 3.85374 9.85874 3.85374 10.4097 4.43878L18 12.4994L10.4097 20.56C10.1366 20.855 9.77398 21 9.41142 21Z"
      fill="#303030"
    />
  </svg>
);
