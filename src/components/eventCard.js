import moment from 'moment';
import { useRouter } from 'next/router';
import React from 'react';

import { Constants } from '../config/constants';
import { uriToLang } from '../config/utils';
import { useCountry } from '../providers/countryProvider';
import { useGlobal } from '../providers/globalProvider';
import Styles from '../styles/eventCardStyle.module.css';
import { ChevronRight } from './icons';

const EventCard = ({ event, userId }) => {
  const router = useRouter();
  const lang = uriToLang(router.query.lang || Constants.defaultCountry.lang);
  const { siteSettings } = useGlobal();
  const { genUrl } = useCountry();
  return (
    <div key={event.slugId} className={`col-sm-6 ${Styles.FeatureCard}`}>
      <div className={Styles.cardBackgroundImage}>
        <div className="badgeCard">
          <img src={`/assets/images/${event.category}.svg`} alt="" />
          <p>{Constants.eventTypeDisplayName[event.category]}</p>
        </div>
        <a href={genUrl(`/event/${event.slugId}?id=${userId}`)}>
          <img src={event.imageUrl} alt="" />
        </a>
      </div>
      <div className={Styles.cardAbout}>
        <div className={Styles.cardTitle}>
          {event.featured && (
            <img src="/assets/svgImages/yellowstar.svg" width="22px" alt="" />
          )}
          <a href={genUrl(`/event/${event.slugId}?id=${userId}`)}>
            {event.title}
          </a>
        </div>
        <div className={Styles.cardDescription}>{event.description}</div>

        <div className={Styles.fixedCardBottom}>
          <div className={Styles.addresCard}>
            <div>
              <img
                src="/assets/images/featuresSection/addressIcon.svg"
                width="24px"
                alt=""
              />
            </div>
            <div className={Styles.location}>
              {event.location},&nbsp;{event.venueAddress}
            </div>
            <div className={Styles.datetim}>
              {moment(event.startDate).format('YYYY/MM/DD')},{' '}
              {moment(event.startDate).format('HH:mm')}
            </div>
          </div>
          <a
            href={genUrl(`/event/${event.slugId}?id=${userId}`)}
            className={Styles.goToEvent}
          >
            {siteSettings?.terms[lang]?.goToEvent}
            <div className={Styles.chevronRightIcon}>
              <ChevronRight />
            </div>
          </a>
        </div>
      </div>
    </div>
  );
};

export default EventCard;
