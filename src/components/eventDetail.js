import moment from 'moment';
import React from 'react';

import styles from '../styles/eventDetail.module.css';

const EventDetail = ({ event }) => {
  return (
    <div className="position-relative">
      <div className={styles.eventDetails}>
        <h2>{event.title}</h2>
        <label className="sustainability" htmlFor="">
          {event.category}
        </label>
        {event.tag && (
          <div className={`tag ${event.tag.toLowerCase()}`}>
            {' '}
            <span>{event.tag}</span>
          </div>
        )}
        <div className={`mb-2 ${styles.addresCard}`}>
          <div>
            <img
              src="/assets/images/featuresSection/addressIcon.svg"
              width="24px"
              alt=""
            />
          </div>
          <div className={styles.location}>
            <>
              {event.location},&nbsp;{event.venueAddress}
              {event.location?.street}{' '}
              {event.location?.town ? ', ' + event.location?.town : ''}{' '}
              {event.location?.zip ? ', ' + event.location?.zip : ''}
            </>
            <br />
            {moment(event.startDate).format('YYYY/MM/DD')},{' '}
            {moment(event.startDate).format('HH:mm')}
          </div>
        </div>
        <div className={styles.cardDescription}>{event.longDescription}</div>
        <a href={event.signUpLink} title="" className="btn themeButton mt-4">
          Get ticket
        </a>
      </div>
    </div>
  );
};

export default EventDetail;
