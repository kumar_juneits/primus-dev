import React from 'react';

const SubMenuGenerator = ({ menuTitle, url, section, userId }) => {
  let subMenuTitles = [];
  if (menuTitle == 'Events') {
    subMenuTitles = [
      { title: 'Store', url: '', section: '' },
      { title: 'Events', url: url + `?id=${userId}`, section: section }
    ];
  } else if (menuTitle == 'People') {
    subMenuTitles = [
      {
        title: 'Fjällräven guides',
        url: url + `?category=guide&id=${userId}`,
        section: ''
      },
      {
        title: 'Ambassadors',
        url: url + `?category=ambassador&id=${userId}`,
        section: ''
      },
      {
        title: 'Fjällräven alumni',
        url: url + `?category=alumni&id=${userId}`,
        section: ''
      },
      { title: 'Organisations', url: '', section: '' },
      { title: 'Become a Fjällräven Guide', url: '', section: '' }
    ];
  } else if (menuTitle == 'Articles') {
    subMenuTitles = [
      {
        title: 'Sustainability',
        url: url + `?category=sustainability&id=${userId}`,
        section: ''
      },
      {
        title: 'Guides',
        url: url + `?category=guide&id=${userId}`,
        section: section
      },
      {
        title: 'Products',
        url: url + `?category=product&id=${userId}`,
        section: section
      },
      { title: 'Videos', url: '', section: section },
      {
        title: 'Adventures',
        url: url + `?category=adventure&id=${userId}`,
        section: section
      }
    ];
  } else if (menuTitle == 'Shop') {
    subMenuTitles = [];
  }

  return (
    <ul>
      {subMenuTitles &&
        subMenuTitles.map((subMenu, key) => (
          <li key={key}>
            <a
              className="nav-link-submenu-li"
              href={`${
                subMenu.url.length != '' ? subMenu.url : '/?location=mapSection'
              }`}
            >
              {subMenu.title}
            </a>
          </li>
        ))}
    </ul>
  );
};

export default SubMenuGenerator;
