import React from 'react';

import { useCountry } from '../providers/countryProvider';
import styles from '../styles/peopleTopBanner.module.css';
import BackButton from './backButton';
import PageBreadcrumb from './PageBreadcrumb';

const EventTopBanner = ({ event }) => {
  const { genUrl } = useCountry();
  const breadcrumbs = [
    { link: genUrl('/'), label: 'Home' },
    { link: genUrl('/events'), label: 'All upcoming Event' },
    { label: event.title }
  ];

  return (
    <div className="position-relative">
      <div className={styles.topBanner}>
        <img src={event.imageUrl} alt="" />
      </div>
      <div
        className={`position-absolute w-100 m-auto ${styles.breadcrumbContent}`}
      >
        <PageBreadcrumb
          breadcrumbs={breadcrumbs}
          className={'breadcrumbWhite'}
        />
        <BackButton href={genUrl(`event`)} />
      </div>
    </div>
  );
};

export default EventTopBanner;
