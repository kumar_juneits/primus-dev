import React from 'react';

import styles from '../../src/styles/listTag.module.css';

const ListTag = ({ tag, onChange }) => {
  return (
    <div className={styles.tagLabel}>
      <input
        type="checkbox"
        id={`${tag.id}_tag`}
        name="tag"
        onChange={e => onChange(tag, e.target.checked)}
      />
      <label>
        {tag.title}{' '}
        <span>
          {' '}
          <img src="/assets/images/close.svg" alt="" />
        </span>
      </label>
    </div>
  );
};

export default ListTag;
