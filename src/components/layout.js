import React, { useState } from 'react';

import { CountryProvider } from '../providers/countryProvider';
import { GlobalProvider } from '../providers/globalProvider';
import Footer from './footer';
import Header from './header';

const Layout = ({
  children,
  siteSettings,
  countries,
  menuItems,
  userId,
  cartDataResponse
}) => {
  const [containerClass, setContainerClass] = useState('');
  return (
    <GlobalProvider
      siteSettingInit={siteSettings}
      updateContainerClass={setContainerClass}
      token={userId}
    >
      <CountryProvider countriesInit={countries}>
        <div className={`page-container ${containerClass}`}>
          <Header
            menuItems={menuItems}
            userId={userId}
            cartDataResponse={cartDataResponse}
          />
          <div className={'page-body'}>{children}</div>
          <Footer />
        </div>
      </CountryProvider>
    </GlobalProvider>
  );
};
export default Layout;
