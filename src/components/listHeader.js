import * as _ from 'lodash';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

import styles from '../../src/styles/listHeader.module.css';
import { Constants } from '../config/constants';
import { sanityUrlFor } from '../config/sanityUrlFor';
import { getMonths, uriToLang } from '../config/utils';
import { useCountry } from '../providers/countryProvider';
import { useGlobal } from '../providers/globalProvider';
import BackButton from './backButton';
import ListTag from './listTag';
import PageBreadcrumb from './PageBreadcrumb';

const ListHeader = ({ title, subtitle, type, filterCallback, imgURL }) => {
  const router = useRouter();
  const { siteSettings, isMobile } = useGlobal();
  const isEventListing = type === 'event';
  const isArticleListing = type === 'article';
  const isPeopleListing = type === 'people';
  const [filterTags, setFilterTags] = useState([]);
  const [selectedFilterTags, setSelectedFilterTags] = useState([]);
  const [selectedMonth, setSelectedMonth] = useState();
  const [selectedLocation, setSelectedLocation] = useState();
  const months = getMonths(12);
  const { currentCountryData, locations } = useCountry();
  const lang = uriToLang(router.query.lang || Constants.defaultCountry.lang);
  const breadcrumbs = [
    { link: '/', label: siteSettings?.terms[lang]?.home },
    { label: title }
  ];

  const debouncedFetchApi = _.debounce(
    term => filterCallback({ searchTerm: term }),
    1000
  );

  const search = term => {
    debouncedFetchApi(term);
    // filterCallback({ searchTerm: term });
  };
  const monthChange = (m, index) => {
    filterCallback({ month: m.month, year: m.year });
    setSelectedMonth(index);
  };
  const locationChange = loc => {
    filterCallback({ stateId: loc.id });
    setSelectedLocation(loc.id);
  };

  useEffect(() => {
    if (isEventListing) {
      setFilterTags([
        {
          id: Constants.eventTypes.store,
          title: siteSettings.eventCategory[lang]?.store
        },
        {
          id: Constants.eventTypes.campfireEvent,
          title: siteSettings.eventCategory[lang]?.campfireEvent
        },
        {
          id: Constants.eventTypes.fjallravenClassic,
          title: siteSettings.eventCategory[lang]?.fjallravenClassic
        },
        {
          id: Constants.eventTypes.classicEvent,
          title: siteSettings.eventCategory[lang]?.classicEvent
        },
        {
          id: Constants.eventTypes.polarEvent,
          title: siteSettings.eventCategory[lang]?.polarEvent
        },
        {
          id: Constants.eventTypes.brandStoreEvent,
          title: siteSettings.eventCategory[lang]?.brandStoreEvent
        }
      ]);
    }
    if (isPeopleListing) {
      setFilterTags([
        {
          id: Constants.peopleFilterTypes.guide,
          title: siteSettings.peopleCategory[lang]?.fjallravenGuide
        },
        {
          id: Constants.peopleFilterTypes.ambassador,
          title: siteSettings.peopleCategory[lang]?.ambassador
        },
        {
          id: Constants.peopleFilterTypes.alumni,
          title: siteSettings.peopleCategory[lang]?.eventAlumni
        }
      ]);
    }
    if (isArticleListing) {
      setFilterTags([
        {
          id: Constants.articleFilterTypes.adventure,
          title: siteSettings.articleCategory[lang]?.adventure
        },
        {
          id: Constants.articleFilterTypes.guide,
          title: siteSettings.articleCategory[lang]?.guide
        },
        {
          id: Constants.articleFilterTypes.sustainability,
          title: siteSettings.articleCategory[lang]?.sustainability
        },
        {
          id: Constants.articleFilterTypes.product,
          title: siteSettings.articleCategory[lang]?.product
        }
      ]);
    }
  }, []);

  useEffect(() => {
    filterCallback({ category: selectedFilterTags });
  }, [selectedFilterTags]);

  const tagChange = (tag, checked) => {
    if (checked) {
      setSelectedFilterTags([...selectedFilterTags, tag.id]);
    } else {
      let selectedFilters = [...selectedFilterTags];
      let index = selectedFilters.indexOf(tag.id);
      if (index > -1) {
        selectedFilters.splice(index, 1);
        setSelectedFilterTags(selectedFilters);
      }
    }
  };

  useEffect(() => {
    if (router.query.category) {
      setTimeout(() => {
        if (
          document.querySelector(
            '#' + router.query.category.trim() + '_tag'
          ) !== null
        )
          document.getElementById(
            router.query.category.trim() + '_tag'
          ).checked = true;
        tagChange({ id: router.query.category }, true);
      }, 2000);
    }
  }, []);

  return (
    <div
      className={
        styles.pageListHeader +
        (isArticleListing ? ' ' + styles.noTopPadding : '')
      }
    >
      {isArticleListing && (
        <div className={styles.heroSection}>
          <img
            src={sanityUrlFor(imgURL[lang]?.asset, currentCountryData.code)
              .width(600)
              .url()}
            alt=""
            className="img-fluid fadeIn cssanimation"
          />
          <div
            className={`position-absolute w-100 m-auto ${styles.breadcrumbContent}`}
          >
            <PageBreadcrumb
              breadcrumbs={breadcrumbs}
              className={'breadcrumbWhite'}
            />
            <BackButton />
          </div>
          <div
            className={`position-absolute w-100 m-auto ${styles.bannerContent}`}
          >
            <div className="container text-center">
              {isArticleListing ? (
                <h2
                  style={{ color: '#ffff' }}
                  className="fadeInBottom cssanimation"
                >
                  {title}
                </h2>
              ) : (
                <h2 className="fadeInBottom cssanimation">{title}</h2>
              )}
              <p className="bannerTextSmall fadeInBottom cssanimation ">
                {subtitle}
              </p>
            </div>
          </div>
        </div>
      )}

      {isPeopleListing ? (
        <div className="container">
          {!isArticleListing && (
            <>
              <h2>{title}</h2>
              <p style={{ color: '#303030' }}>{subtitle}</p>
            </>
          )}
          <div
            className={`d-flex flex-wrap ${
              isMobile
                ? styles.justifyContentLeftMobile
                : 'justify-content-center'
            }`}
          >
            <div className={'d-flex dropdown-filter'}>
              <div className="mr-2 flex-fill">
                <div className={`dropdown ${styles.whiteDropdown}`}>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      <img
                        src="/assets/images/icons/location.svg"
                        alt="flag"
                        className="mx-2"
                      />{' '}
                      {siteSettings?.terms[lang]?.location}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      {locations &&
                        locations.map(loc => (
                          <Dropdown.Item
                            key={loc.id}
                            eventKey={loc.id}
                            onSelect={() => locationChange(loc)}
                            className={
                              loc.id === selectedLocation ? 'active' : ''
                            }
                          >
                            {loc.stateName}
                          </Dropdown.Item>
                        ))}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </div>
            <div className={`search-filter ${styles.searchFilterContainer}`}>
              <input
                type="text"
                className={`${styles.searchBox} search-filter-input`}
                placeholder={_.upperCase(
                  siteSettings.terms ? siteSettings.terms[lang]?.search : ''
                )}
                onChange={e => search(e.target.value)}
              />
            </div>
          </div>
          <div className="py-3">
            <div
              className={`d-flex flex-wrap ${
                isMobile
                  ? styles.justifyContentLeftMobile
                  : 'justify-content-center'
              }`}
            >
              {filterTags.map(tag => (
                <ListTag tag={tag} key={tag.id} onChange={tagChange} />
              ))}
            </div>
          </div>
        </div>
      ) : (
        <div className="container">
          {!isArticleListing && (
            <>
              <h2>{title}</h2>
              <p style={{ color: '#303030' }}>{subtitle}</p>
            </>
          )}
          <div
            className={`d-flex flex-wrap ${
              isMobile
                ? styles.justifyContentLeftMobile
                : 'justify-content-center'
            }`}
          >
            <div className={`search-filter ${styles.searchFilterContainer}`}>
              <input
                type="text"
                className={`${styles.searchBox} search-filter-input`}
                placeholder={_.upperCase(
                  siteSettings.terms ? siteSettings.terms[lang]?.search : ''
                )}
                onChange={e => search(e.target.value)}
              />
            </div>
            <div className={'d-flex dropdown-filter'}>
              {isEventListing && (
                <div className="mr-2 flex-fill">
                  <div className={`dropdown ${styles.whiteDropdown}`}>
                    <Dropdown>
                      <Dropdown.Toggle variant="success" id="dropdown-basic">
                        <img
                          src="/assets/images/icons/date.svg"
                          alt="flag"
                          className="mx-2"
                        />{' '}
                        {siteSettings?.terms[lang]?.month}
                      </Dropdown.Toggle>

                      <Dropdown.Menu>
                        {months.map((m, index) => (
                          <React.Fragment key={m.year + m.month}>
                            {(index === 0 ||
                              (index > 0 &&
                                months[index - 1].year !== m.year)) && (
                              <Dropdown.Header
                                className={styles.dropdownHeader}
                              >
                                {m.year}:
                              </Dropdown.Header>
                            )}
                            <Dropdown.Item
                              eventKey={m.year + m.month}
                              onSelect={() => monthChange(m, index)}
                              className={
                                selectedMonth === index ? 'active' : ''
                              }
                            >
                              {m.monthDisplayName}
                            </Dropdown.Item>
                          </React.Fragment>
                        ))}
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </div>
              )}
              <div className="mr-2 flex-fill">
                <div className={`dropdown ${styles.whiteDropdown}`}>
                  <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                      <img
                        src="/assets/images/icons/location.svg"
                        alt="flag"
                        className="mx-2"
                      />{' '}
                      {siteSettings?.terms[lang]?.location}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      {locations &&
                        locations.map(loc => (
                          <Dropdown.Item
                            key={loc.id}
                            eventKey={loc.id}
                            onSelect={() => locationChange(loc)}
                            className={
                              loc.id === selectedLocation ? 'active' : ''
                            }
                          >
                            {loc.stateName}
                          </Dropdown.Item>
                        ))}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </div>
          </div>
          <div className="py-3">
            <div
              className={`d-flex flex-wrap ${
                isMobile
                  ? styles.justifyContentLeftMobile
                  : 'justify-content-center'
              }`}
            >
              {filterTags.map(tag => (
                <ListTag tag={tag} key={tag.id} onChange={tagChange} />
              ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ListHeader;
