import React from 'react';

import { capitalizeFirstLetter, getLangUrl } from '../config/utils';

const MobileDropMenu = ({
  data,
  showRegionPicker,
  menuItems,
  langCode,
  countryCode
}) => {
  const getDefaultLangValue = languageUrlDictionary => {
    for (let key in languageUrlDictionary) {
      let url = languageUrlDictionary[key];
      if (url) {
        let path = getLangUrl(url);
        if (path && path.indexOf(langCode) > -1) {
          return path;
        }
      }
    }
    return '';
  };

  const onLangChange = event => {
    let base_url = window.location.origin;
    window.location = base_url + event.target.value;
  };

  return (
    <>
      <div className="header--drop-wrapper">
        <nav className="header--drop">
          <div
            open="open"
            close="Close"
            className="input--root header--drop-input"
          >
            <input
              placeholder="Search"
              aria-label="Search text"
              className="input--input"
            />
            <button
              aria-label="Submit"
              className="input--submit js-mobile-search-button"
            />
          </div>
          {menuItems &&
            menuItems.map((menu, index) => (
              <div className="menu--root" key={index}>
                <a href={menu.url[langCode]} className="menu--title">
                  {menu.title[langCode]}
                </a>
              </div>
            ))}
          <br /> <br />
          {data && (
            <ul className="header--tertiary-drop-items">
              {data.topNavigationSiteLinks &&
                data.topNavigationSiteLinks.map((link, index) => (
                  <li key={index}>
                    <a className="link--root" href={link.link}>
                      {link.htmlIcon && (
                        <div
                          className="link--icon"
                          dangerouslySetInnerHTML={{ __html: link.htmlIcon }}
                        />
                      )}{' '}
                      {link.icon && (
                        <img src={link.icon} alt="" className="link--icon" />
                      )}
                      <span>{link.label}</span>
                    </a>
                  </li>
                ))}
              {data.currentMarket && (
                <>
                  <li>
                    <a
                      href="#"
                      className="link--root header--region"
                      onClick={showRegionPicker}
                    >
                      <span
                        aria-expanded="false"
                        className="region--country top-navigation"
                      >
                        <span
                          className={`flag-icon ${data.currentMarket.flag} region--country-flag`}
                        />{' '}
                        <span>
                          <span className="region--country-title">
                            {data.currentMarket.marketName}
                            <span className="region--country-currency">
                              | {data.currentMarket.currencySymbol}
                            </span>
                          </span>
                        </span>
                      </span>
                    </a>
                  </li>
                  <li>
                    <span className="link--root arrow-link--root region--topnav-lang-area">
                      <select
                        className="region--topnav-lang-select"
                        aria-label="Select site language"
                        name="languageUrls"
                        onChange={onLangChange}
                        defaultValue={getDefaultLangValue(
                          data.currentMarket.languageUrlDictionary
                        )}
                      >
                        {data.currentMarket &&
                          Object.keys(
                            data.currentMarket.languageUrlDictionary
                          ).map(key => (
                            <option
                              key={key}
                              value={getLangUrl(
                                data.currentMarket.languageUrlDictionary[key]
                              )}
                            >
                              {capitalizeFirstLetter(key)}
                            </option>
                          ))}
                      </select>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 21.24 34"
                        focusable="false"
                        aria-hidden="true"
                        className="arrow-link--icon"
                      >
                        <path
                          d="M3,34a3,3,0,0,1-2.12-.88,3,3,0,0,1,0-4.24L12.76,17,.88,5.12A3,
                                        3,0,0,1,5.12.88L21.24,17,5.12,33.12A3,3,0,0,1,3,34Z"
                        />
                      </svg>
                    </span>
                  </li>

                  <li>
                    <a
                      href={`/${countryCode}/${langCode}/my-account/login`}
                      className="link--root"
                    >
                      <span>Account</span>
                    </a>
                  </li>
                </>
              )}
            </ul>
          )}
        </nav>
      </div>
    </>
  );
};

export default MobileDropMenu;
