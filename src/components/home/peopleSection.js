/* eslint-disable react/jsx-key */
import 'react-multi-carousel/lib/styles.css';

import React from 'react';

import { sanityUrlFor } from '../../config/sanityUrlFor';
import { useCountry } from '../../providers/countryProvider';
import Styles from '../../styles/home/peopleSectionStyle.module.css';
import Tagline from '../tagline';

function PeopleSection({ data, chefList }) {
  // const [people, setPeople] = useState([]);
  const { genUrl } = useCountry();

  /* useEffect(() => {
    fetchPeople().then();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchPeople = async () => {
    const apiRes = await axios.post(
      `${Constants.basePath.PEOPLE}/orchestration/people/search`,
      {
        countryCode: router.query.code || Constants.defaultCountry.code,
        page: 0,
        size: 16
      }
    );
    if (apiRes && apiRes.data && apiRes.data.data.peopleList) {
      setPeople(apiRes.data.data.peopleList);
    }
  }; */

  return (
    <>
      {data && (
        <section className="container">
          <div className={Styles.outerDiv}>
            <div
              className={`d-flex justify-content-center pt-5 ${Styles.responsiveTopFix}`}
            >
              <div className={Styles.title}>{data.heading}</div>
            </div>

            <div className="d-flex justify-content-center">
              <div className={Styles.subTitle}>
                <Tagline tagline={data.tagline} />
              </div>
            </div>
            <div className="row mb-3 pt-4">
              {chefList &&
                chefList.map(value => {
                  return (
                    <div className="col-sm-4 card_padd">
                      <div className={`mb-3 ${Styles.imageBox}`}>
                        <a className="hoverImage" href={'/'}>
                          <img
                            className={Styles.chefImage}
                            alt={''}
                            src={sanityUrlFor(value.image?.asset)}
                          />
                        </a>
                        <div className={Styles.bottomDiv}>
                          <span>Chef</span>
                          <h3>{value.name}</h3>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
          <div className={`pt-5 pb-5 ${Styles.footerButton}`}>
            {data.cta && (
              <a
                href={genUrl(`people`)}
                title=""
                className="btn themeButton m-auto"
              >
                {data.cta.title}
              </a>
            )}
          </div>
        </section>
      )}
    </>
  );
}

export default PeopleSection;
