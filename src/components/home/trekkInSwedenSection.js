import React from 'react';

import { sanityUrlFor } from '../../config/sanityUrlFor';
import { useCountry } from '../../providers/countryProvider';
import styles from '../../styles/home/trekkInSwedenSectionStyle.module.css';

const TrekkInSwedenSection = ({ products, learnMore }) => {
  const { currentCountryData } = useCountry();
  return (
    <>
      {((products && products.length > 0) || learnMore) && (
        <div className="container">
          <div className="row mt-3">
            {products && products.length > 0 && (
              <>
                {products.map((product, index) => (
                  <React.Fragment key={product.title}>
                    {!product.disabled && (
                      <div
                        key={index}
                        className={
                          products.length >= 0 && learnMore
                            ? 'col-sm-6'
                            : 'col-sm-12'
                        }
                      >
                        <div className={styles.shopBox}>
                          <h2>
                            {product.title} {currentCountryData.name}
                          </h2>
                          <div className={`d-flex mt-5 ${styles.shopFlex}`}>
                            <div className={`w-50 ${styles.width100}`}>
                              <img
                                src={sanityUrlFor(product.image?.asset)}
                                className={styles.shopImage}
                              />
                            </div>
                            <div className={`w-50 ${styles.width100}`}>
                              <div className={styles.descText}>
                                <h3>{product.name}</h3>
                                <h4>{product.price}</h4>
                                <p>{product.description}</p>
                                <div className="d-flex mb-4 mt-4">
                                  <label
                                    className={styles.radioContainer}
                                    htmlFor={'radio1'}
                                  >
                                    <input
                                      type="radio"
                                      name="radio"
                                      id={'radio1'}
                                    />
                                    <span
                                      className={`${styles.checkMark} ${styles.color1}`}
                                    ></span>
                                  </label>
                                  <label
                                    className={styles.radioContainer}
                                    htmlFor={'radio2'}
                                  >
                                    <input
                                      type="radio"
                                      name="radio"
                                      id={'radio2'}
                                    />
                                    <span
                                      className={`${styles.checkMark} ${styles.color2}`}
                                    ></span>
                                  </label>
                                  <label
                                    className={styles.radioContainer}
                                    htmlFor={'radio3'}
                                  >
                                    <input
                                      type="radio"
                                      name="radio"
                                      id={'radio3'}
                                    />
                                    <span
                                      className={`${styles.checkMark} ${styles.color3}`}
                                    ></span>
                                  </label>
                                  <label
                                    className={styles.radioContainer}
                                    htmlFor={'radio4'}
                                  >
                                    <input
                                      type="radio"
                                      name="radio"
                                      id={'radio4'}
                                    />
                                    <span
                                      className={`${styles.checkMark} ${styles.color4}`}
                                    ></span>
                                  </label>
                                  <label
                                    className={styles.radioContainer}
                                    htmlFor={'radio5'}
                                  >
                                    <input
                                      type="radio"
                                      name="radio"
                                      id={'radio5'}
                                    />
                                    <span
                                      className={`${styles.checkMark} ${styles.color5}`}
                                    ></span>
                                  </label>
                                </div>

                                <a
                                  href={product.cta?.route}
                                  className={
                                    'btn themeButton ' + styles.primaryBtn
                                  }
                                >
                                  {product.cta?.title}
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </React.Fragment>
                ))}
              </>
            )}

            {learnMore && (
              <div
                className={
                  products && products.length > 0
                    ? 'col-sm-6 pl-0'
                    : 'col-sm-12'
                }
              >
                <div className="position-relative h-100 hoverImage rounderDiv">
                  <a className="hoverImage  h-100">
                    <img
                      src={sanityUrlFor(learnMore.image?.asset)}
                      className={styles.bigImage}
                    />
                  </a>
                  <div className={styles.centerDiv}>
                    <h3>{learnMore.title}</h3>
                    <p>{learnMore.subtitle}</p>
                    <a
                      href={learnMore.cta?.route}
                      className={`btn themeButton buttonWhite mt-5 mx-auto ${styles.primaryBtn}`}
                    >
                      {learnMore.cta?.title}
                    </a>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default TrekkInSwedenSection;
