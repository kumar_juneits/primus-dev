import React from 'react';

import { sanityUrlFor } from '../../config/sanityUrlFor';
import styles from '../../styles/home/featuredVideosSection.module.css';

const FeaturedVideosSection = ({
  switchVideo,
  featuredVideos,
  featuredPlaylists
}) => {
  return (
    <div className={styles.featuredVideo}>
      <div className={styles.featuredVideoScroll}>
        {featuredVideos && <h2>Featured videos ({featuredVideos.length})</h2>}
        {featuredVideos &&
          featuredVideos.length > 0 &&
          featuredVideos.map((v, index) => (
            <div
              className={
                styles.featuredDiv +
                (index === featuredVideos.length - 1
                  ? ' ' + styles.noBorder
                  : '')
              }
              key={index}
            >
              <div
                className="w-25"
                onClick={() => switchVideo('embed/' + v.videoCode)}
              >
                <img src={sanityUrlFor(v.backgroundImage?.asset)} />
              </div>
              <div className="w-75">
                <h3 onClick={() => switchVideo('embed/' + v.videoCode)}>
                  {v.heading}
                </h3>
                <p>{v.subtitle}</p>
              </div>
            </div>
          ))}

        {featuredPlaylists && (
          <h2>Featured Playlists ({featuredPlaylists.length})</h2>
        )}
        {featuredPlaylists &&
          featuredPlaylists.length > 0 &&
          featuredPlaylists.map((v, index) => (
            <div className={styles.featuredDiv} key={index}>
              <div
                className="w-25"
                onClick={() =>
                  switchVideo('embed/videoseries/?list=' + v.videoCode)
                }
              >
                <img src={sanityUrlFor(v.backgroundImage?.asset)} />
              </div>
              <div className="w-75">
                <h3
                  onClick={() =>
                    switchVideo('embed/videoseries/?list=' + v.videoCode)
                  }
                >
                  {v.heading}
                </h3>
                <p>{v.subtitle}</p>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

export default FeaturedVideosSection;
