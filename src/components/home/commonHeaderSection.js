import React from 'react';

import Styles from '../../styles/home/commonSectionHeaderStyle.module.css';
import Tagline from '../tagline';

function CommonHeaderSection({ data }) {
  return (
    <>
      {data && (
        <section className="container pl-0">
          <div className="d-flex justify-content-center">
            <div className={Styles.title}>{data.heading}</div>
          </div>

          <div className="d-flex justify-content-center">
            <div className={Styles.subTitle}>
              <Tagline tagline={data.tagline} />
            </div>
          </div>
        </section>
      )}
    </>
  );
}

export default CommonHeaderSection;
