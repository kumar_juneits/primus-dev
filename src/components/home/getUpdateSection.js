// import axios from 'axios';
// import * as _ from 'lodash';
import React, { useState } from 'react';
const BlockContent = require('@sanity/block-content-to-react');
// import { Constants } from '../../config/constants';
import { sanityUrlFor } from '../../config/sanityUrlFor';
import styles from '../../styles/home/getUpdateSectionStyle.module.css';

const GetUpdateSection = ({ data }) => {
  const [mailHandler, setMailHandler] = useState('');
  const mailGrasper = event => {
    setMailHandler(event.target.value);
  };
  const emailSubmission = event => {
    event.preventDefault();
    // const getMail = {
    //   email: mailHandler
    // };
    subScripeUser();

    setMailHandler('');
  };

  const subScripeUser = async () => {
    /* let dataSend = JSON.stringify({
      brand: 'fjallraven',
      marketId: _.upperCase(countryCode),
      email: mailHandler,
      registrationSouce: 'BEP'
    });

    let config = {
      method: 'post',
      url: Constants.basePath.SUBSCRIBEUSER,
      headers: {
        'Content-Type': 'application/json'
      },
      data: dataSend
    };

    const apiRes = await axios(config);
    if (apiRes && apiRes.data) {
      console.log('api Response', apiRes);
    } */
  };
  return (
    <>
      {data && (
        <section className={'mb-5'}>
          <div className="container pt-4 pb-4">
            <div className={`d-flex  ${styles.card}`}>
              <div
                className={styles.cardImgTop}
                style={{
                  backgroundImage: `url(${sanityUrlFor(
                    data.backgroundImage?.asset
                  )})`
                }}
              />
              <div
                className={`text-left pl-3 py-5 d-flex align-items-center position-relative justify-content-center ${styles.cardBody}`}
              >
                <div className={`py-5 ${styles.formBox}`}>
                  <span className={styles.cardTitle}>{data.heading}</span>
                  <p className={styles.cardText}>{data.subtitle}</p>
                  <div className={styles.emailInput}>
                    <input
                      value={mailHandler}
                      onChange={mailGrasper}
                      type="email"
                      className={styles.emailSection}
                      placeholder={data.inputPlaceholder}
                    />
                    <img
                      src="/assets/images/sectionThree/RightArrow.svg"
                      alt=""
                      width="8px"
                      onClick={emailSubmission}
                      type="submit"
                      className={styles.modalArrow}
                    />
                  </div>
                  <div className={styles.terms}>
                    <label className={styles.checkboxDiv}>
                      <BlockContent blocks={data.termText} />
                      <input type="checkbox" />
                      <span className={styles.checkMark} />
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
};

export default GetUpdateSection;
