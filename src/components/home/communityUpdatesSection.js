import React from 'react';

import styles from '../../styles/home/communityUpdatesSection.module.css';
import FlowboxScript from '../flowboxScript';
import Tagline from '../tagline';

const CommunityUpdatesSection = ({ data }) => {
  return (
    <>
      {data && (
        <div className={styles.communityUpdateSection}>
          <div className="container">
            <div className="d-flex justify-content-center">
              <div>
                <div className="sectiontitle">{data.heading}</div>
                <p className="intro">
                  <Tagline tagline={data.tagline} />{' '}
                </p>
              </div>
            </div>
            <FlowboxScript />
          </div>
        </div>
      )}
    </>
  );
};

export default CommunityUpdatesSection;
