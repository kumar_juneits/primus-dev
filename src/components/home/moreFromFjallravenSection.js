import * as _ from 'lodash';
import React from 'react';

import { sanityUrlFor } from '../../config/sanityUrlFor';
import { useCountry } from '../../providers/countryProvider';
import Styles from '../../styles/home/featuredEventSectionStyle.module.css';
import styles from '../../styles/home/moreFromFjallravenSectionStyle.module.css';
import Tagline from '../tagline';

const MoreFromFjallravenSection = ({ data, posts }) => {
  // const [posts, setPosts] = useState([]);
  const { genUrl } = useCountry();
  const { currentCountryData } = useCountry();
  /* useEffect(() => {
    fetchBlogs().then();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchBlogs = async () => {
    const apiRes = await axios.post(
      `${Constants.basePath.BLOG}/orchestration/blog/search`,
      {
        countryCode: router.query.code || Constants.defaultCountry.code,
        page: 0,
        size: Config.homepageBlogItemCount
      }
    );
    if (apiRes && apiRes.data && apiRes.data.data) {
      setPosts(apiRes.data.data.blogList);
    }
  }; */

  return (
    <>
      {data && (
        <>
          <div className={styles.communityUpdateSection}>
            <div className="container pt-4">
              <div className="d-flex justify-content-center">
                <div>
                  <div className="sectiontitle">{data.heading}</div>
                  <p className="intro">
                    <Tagline tagline={data.tagline} />
                  </p>
                </div>
              </div>
              {posts && posts.length > 0 && (
                <div className="row mb-3">
                  <div className="col-sm-6 responsiveMargin">
                    <div className={`h-100 ${styles.imageBox}`}>
                      <a className="hoverImage h-100" href="/">
                        <img
                          src={sanityUrlFor(
                            posts[0].mainImage?.asset,
                            currentCountryData.code
                          )
                            .width(600)
                            .url()}
                          alt=""
                          className={styles.bigImage}
                        />
                      </a>
                      <div className={styles.bottomDiv}>
                        <div className={'tag ' + ''}>
                          <span>{}</span>
                        </div>
                        <h3>{posts[0].title}</h3>
                        <a href="/" className="btn themeButton buttonWhite">
                          read more
                        </a>
                      </div>
                    </div>
                  </div>
                  {posts && posts.length > 1 && (
                    <div className="col-sm-6 pl-0">
                      <div
                        className={`mb-3 ${styles.imageBox} ${styles.imageBoxMaxHeight}`}
                      >
                        <a className="hoverImage minCardStyle" href={'/'}>
                          <img
                            src={sanityUrlFor(
                              posts[1].mainImage?.asset,
                              currentCountryData.code
                            )
                              .width(600)
                              .url()}
                            alt=""
                            className={styles.bigImage}
                          />
                        </a>
                        <div className={styles.bottomDiv}>
                          <div className={'tag ' + ''}>
                            <span>{}</span>
                          </div>
                          <h3>{posts[1].title}</h3>
                          <a href={'/'} className="btn themeButton buttonWhite">
                            read more
                          </a>
                        </div>
                      </div>
                      {posts && posts.length > 2 && (
                        <div
                          className={`${styles.imageBox} ${styles.imageBoxMaxHeight}`}
                        >
                          <a className="hoverImage minCardStyle" href={'/'}>
                            <img
                              src={sanityUrlFor(
                                posts[2].mainImage?.asset,
                                currentCountryData.code
                              )
                                .width(600)
                                .url()}
                              alt=""
                              className={styles.bigImage}
                            />
                          </a>
                          <div className={styles.bottomDiv}>
                            <div className={'tag ' + ''}>
                              <span>{}</span>
                            </div>
                            <h3>{posts[2].title}</h3>
                            <a
                              href={'/'}
                              className="btn themeButton buttonWhite"
                            >
                              read more
                            </a>
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                </div>
              )}
            </div>

            {data.cta && data.cta.title && (
              <div className={`mt-4 mb-5 ${Styles.footerButton}`}>
                <a
                  href={genUrl(`articles`)}
                  title=""
                  className="btn themeButton"
                >
                  {_.capitalize(data.cta.title)}
                </a>
              </div>
            )}
          </div>
        </>
      )}
    </>
  );
};

export default MoreFromFjallravenSection;
