import * as _ from 'lodash';
import React from 'react';

import { sanityUrlFor } from '../../config/sanityUrlFor';
import { useCountry } from '../../providers/countryProvider';
import Tagline from '../tagline';

const WelcomeSection = ({ data }) => {
  const { currentCountryData } = useCountry();

  const title = _.get(data, 'cta.title', '');
  const routeUrl = _.get(data, 'cta.route', '');

  return (
    <>
      {data && (
        <section
          className={`intro-text mainBanner position-relative mb-5 ${
            data.hideOverlay ? 'mainBannerHideOverlay' : ''
          }`}
        >
          {data.backgroundImage && (
            <img
              src={sanityUrlFor(
                data.backgroundImage?.asset,
                currentCountryData.code
              )
                .width(600)
                .url()}
              alt=""
              className="img-fluid fadeIn cssanimation"
            />
          )}
          <div className="bannerContent position-absolute w-100 m-auto">
            <div className="container text-center">
              {/*  */}
              <h1 className="fadeInBottom cssanimation">{data.heading}</h1>
              <h3 className="bannerTextSmall fadeInBottom cssanimation ">
                <Tagline tagline={data.tagline} />
              </h3>
              {!_.isEmpty(title) && (
                <a
                  href={routeUrl}
                  className="btn themeButton buttonWhite mt-5 mx-auto extraPadding f-14p"
                >
                  {title}
                </a>
              )}
            </div>
          </div>
        </section>
      )}
    </>
  );
};

export default WelcomeSection;
