import 'react-multi-carousel/lib/styles.css';

import axios from 'axios';
import * as _ from 'lodash';
import moment from 'moment';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Carousel from 'react-multi-carousel';

import { Constants } from '../../config/constants';
import { useCountry } from '../../providers/countryProvider';
import Styles from '../../styles/home/featuredEventSectionStyle.module.css';
import { ChevronRight } from '../icons';
import Tagline from '../tagline';

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 3
  },
  xdisktop: {
    breakpoint: { max: 3000, min: 2000 },
    items: 3
  },
  desktop: {
    breakpoint: { max: 2000, min: 1250 },
    items: 3
  },
  tablet: {
    breakpoint: { max: 1250, min: 464 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 786, min: 0 },
    items: 1.3
  }
};

function FeaturedEventSection({ data }) {
  const router = useRouter();
  const { currentCountryData, genUrl } = useCountry();
  const [featureEvents, setFeatureEvents] = useState([]);
  useEffect(() => {
    fetchEvents().then();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchEvents = async () => {
    const eventCountRes = await axios.post(
      `${Constants.basePath.EVENT}/orchestration/event/search`,
      {
        countryCode: router.query.code || Constants.defaultCountry.code,
        page: 0,
        size: 5
      }
    );
    let eventData = [];
    if (
      eventCountRes &&
      eventCountRes.data &&
      eventCountRes.data.data &&
      eventCountRes.data.data.eventList
    ) {
      eventData = eventCountRes.data.data.eventList;
      setFeatureEvents(eventData);
    }
  };

  return (
    <>
      {data && (
        <section className="container pl-0">
          <div className="d-flex justify-content-center">
            <div className={Styles.title}>
              {data.heading} {currentCountryData.name}
            </div>
          </div>

          <div className="d-flex justify-content-center">
            <div className={Styles.subTitle}>
              <Tagline tagline={data.tagline} />
            </div>
          </div>
          <div className={`flex justify-content-center ${Styles.SliderMargin}`}>
            <Carousel responsive={responsive}>
              {typeof featureEvents !== 'undefined' &&
                featureEvents.length > 0 &&
                featureEvents.map(value => {
                  return (
                    <div
                      key={value.id || value.name}
                      className={Styles.FeatureCard}
                    >
                      <div className={Styles.cardBackgroundImage}>
                        <div className="badgeCard">
                          <img
                            src={`/assets/images/${value.category}.svg`}
                            alt=""
                          />
                          <p>
                            {Constants.eventTypeDisplayName[value.category]}
                          </p>
                        </div>
                        <a href={genUrl('event/' + value.id)}>
                          <img
                            src={value.imageUrl}
                            alt=""
                            width="392px"
                            height="261px"
                          />
                        </a>
                      </div>
                      <div className={Styles.cardAbout}>
                        <div className={Styles.cardTitle}>
                          {value.featured && (
                            <img
                              src="/assets/svgImages/yellowstar.svg"
                              width="22px"
                              alt=""
                            />
                          )}
                          <a href={genUrl('event/' + value.id)}>
                            {value.title}
                          </a>
                        </div>
                        <div className={Styles.cardDescription}>
                          {value.description}
                        </div>

                        <div className={Styles.fixedCardBottom}>
                          <div className={Styles.addresCard}>
                            <div>
                              <img
                                src="/assets/images/featuresSection/addressIcon.svg"
                                width="24px"
                                alt=""
                              />
                            </div>
                            <div className={Styles.location}>
                              <>
                                {value.location}, {value.venueAddress}{' '}
                                {value.location?.street}{' '}
                                {value.location?.town
                                  ? ', ' + value.location?.town
                                  : ''}{' '}
                                {value.location?.zip
                                  ? ', ' + value.location?.zip
                                  : ''}
                              </>
                              <br />
                              {moment(value.startDate).format(
                                'YYYY/MM/DD'
                              )}, {moment(value.startDate).format('HH:mm')}
                            </div>
                          </div>

                          <a
                            type="button"
                            href={genUrl('event/' + value.id)}
                            className={Styles.goToEvent}
                          >
                            {data.cardConfig?.linkText}
                            <div className={Styles.chevronRightIcon}>
                              <ChevronRight />
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </Carousel>
          </div>
          {data.cta && data.cta.title && (
            <div className={`mt-4 mb-5 ${Styles.footerButton}`}>
              <a href={genUrl(`events`)} title="" className="btn themeButton">
                {_.capitalize(data.cta.title)}
              </a>
            </div>
          )}
        </section>
      )}
    </>
  );
}

export default FeaturedEventSection;
