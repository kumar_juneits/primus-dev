import React, { useState } from 'react';

import styles from '../../styles/home/recentVideosSectionStyle.module.css';
import Tagline from '../tagline';
import FeaturedVideosSection from './featuredVideosSection';

const RecentVideosSection = ({ data }) => {
  const defaultVideoCode =
    data.featuredVideos && data.featuredVideos.length > 0
      ? 'embed/' + data.featuredVideos[0].videoCode
      : null;
  const [videoCode, setVideoCode] = useState(defaultVideoCode);
  return (
    <>
      {data && (
        <div className="container pb-4 pt-5">
          <div className="d-flex justify-content-center pt-5">
            <div>
              <div className="sectiontitle">{data.heading}</div>
              <p className="intro">
                <Tagline tagline={data.tagline} />
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-8">
              <div className={styles.youtubeBox}>
                {videoCode && (
                  <iframe
                    width="100%"
                    height="530"
                    src={`https://www.youtube.com/${videoCode}`}
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                  ></iframe>
                )}
              </div>
            </div>
            <div className="col-sm-4">
              {(data.featuredVideos || data.featuredPlaylists) && (
                <FeaturedVideosSection
                  switchVideo={setVideoCode}
                  featuredVideos={data.featuredVideos}
                  featuredPlaylists={data.featuredPlaylists}
                />
              )}
            </div>
          </div>
          <div className={`pt-5 pb-5 `}>
            {data.cta && data.cta.title && (
              <a href="" title="" className="btn themeButton m-auto">
                {data.cta.title}
              </a>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default RecentVideosSection;
