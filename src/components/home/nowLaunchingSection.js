import React from 'react';

import { sanityUrlFor } from '../../config/sanityUrlFor';
import styles from '../../styles/home/nowLaunchingSectionStyle.module.css';

const NowLaunchingSection = ({ data }) => {
  return (
    <>
      {data && (
        <div className="container">
          <div className="position-relative hoverImage rounderDiv">
            <a className="hoverImage">
              <img
                alt={''}
                src={sanityUrlFor(data.image?.asset)}
                className={styles.bigImage}
              />
            </a>
            <div className={styles.centerDiv}>
              <h3>{data.title}</h3>
              <p>{data.subtitle}</p>
              <a
                href={data.cta?.route}
                className={
                  'btn themeButton buttonWhite mt-5 mx-auto ' +
                  styles.primaryBtn
                }
              >
                {data.cta?.title}
              </a>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default NowLaunchingSection;
