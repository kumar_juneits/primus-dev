import 'react-multi-carousel/lib/styles.css';

import { useRouter } from 'next/router';
import React from 'react';

import { Constants } from '../config/constants';
import styles from '../styles/peopleDetail.module.css';

const PeopleDetail = ({ people }) => {
  const router = useRouter();
  const countryCode = router.query.code || Constants.defaultCountry.code;
  return (
    <div className={styles.peopleDetail}>
      <img src={people.imgURL} className={styles.peopleImg} alt="" />
      <h2 className={styles.guideName}>
        {people.firstname} {people.lastname}
      </h2>
      <div className="d-flex align-items-center">
        <div className={styles.sustainability}>
          <span>{people.tag}</span>
        </div>
        <div>
          <img
            src={`/assets/flags/${countryCode}.svg`}
            className={styles.flagImg}
            width="24px"
            alt=""
          />
        </div>
        <div className={styles.location}>
          {people.country} | {people.location}
        </div>
        <div className={styles.location}>
          <img
            src={`/assets/socialmediaicons/Facebook.svg`}
            className={styles.socialmedia1}
            width="24px"
            alt=""
          />
          <img
            src={`/assets/socialmediaicons/Instagram.svg`}
            className={styles.socialmedia2}
            width="24px"
            alt=""
          />
        </div>
      </div>
      <div className={styles.cardDescription}>{people.description}</div>
    </div>
  );
};

export default PeopleDetail;
