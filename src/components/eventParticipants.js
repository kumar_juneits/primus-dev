import React from 'react';

import styles from '../styles/eventParticipants.module.css';

const EventParticipants = ({ participant }) => {
  const countryCodeCustomImage =
    participant.countryCode === 'GB' ? 'uk' : participant.countryCode;
  return (
    <div className={styles.peopleSmallCard} key={participant.id}>
      <img alt={''} src={participant.imgURL} />

      <div className="">
        <h2>{participant.firstname + participant.lastname}</h2>
        <div className="d-flex align-items-center">
          <div className={'tag' + ' ' + participant.tag.toLowerCase()}>
            <span>{participant.tag}</span>
          </div>
          <div>
            <img
              src={`/assets/flags/${countryCodeCustomImage}.svg`}
              className={styles.flagImg}
              width="24px"
              alt=""
            />
          </div>
          <div className={styles.location}>{participant.country}</div>
        </div>
      </div>
    </div>
  );
};

export default EventParticipants;
