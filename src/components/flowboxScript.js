import Head from 'next/head';
import { useRouter } from 'next/router';
import Script from 'next/script';
import React from 'react';

import { Constants } from '../config/constants';

const FlowboxScript = ({ flowBoxKey }) => {
  const router = useRouter();
  const countryCode = router.query.code || Constants.defaultCountry.code;
  const flowboxCountry = Constants.flowboxCountryMapping[countryCode];

  let flowboxLang = Constants.defaultCountry.lang;
  let flowboxKey = Constants.flowboxCountryMapping.uk.key;

  if (flowboxCountry) {
    if (flowboxCountry.lang) {
      flowboxLang = flowboxCountry.lang;
    } else {
      flowboxLang = flowboxCountry.lang;
    }
    flowboxKey = flowBoxKey || flowboxCountry.key;
  } else if (router.query.code) {
    flowboxLang = router.query.lang;
  }

  return (
    <>
      <Head>
        <script
          dangerouslySetInnerHTML={{
            __html: `
        (function(d, id) {
        if (!window.flowbox) { var f = function () { f.q.push(arguments); }; f.q = []; window.flowbox = f; }
        if (d.getElementById(id)) {return;}
        var s = d.createElement('script'), fjs = d.scripts[d.scripts.length - 1]; s.id = id; s.async = true;
        s.src = 'https://connect.getflowbox.com/flowbox.js';
        fjs.parentNode.insertBefore(s, fjs);
      })(document, 'flowbox-js-embed');
                  `
          }}
        ></script>
      </Head>
      <div id="js-flowbox-flow" />
      <Script strategy="lazyOnload">
        {`window.flowbox('init', {
        container: '#js-flowbox-flow',
        key: '${flowboxKey}',
        locale: '${flowboxLang}'
      })`}
      </Script>
    </>
  );
};

export default FlowboxScript;
