import React from 'react';
const Tagline = ({ tagline }) => {
  return (
    <>
      {tagline &&
        tagline.length > 0 &&
        tagline.length > 0 &&
        tagline.map((line, index) => {
          return (
            <span key={index}>
              {line.children.map((lineChild, indexInner) => (
                <span key={indexInner}>
                  <span className="">{lineChild.text}</span>
                  <br />
                </span>
              ))}
            </span>
          );
        })}
    </>
  );
};

export default Tagline;
