import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

import { Constants } from '../config/constants';
import { capitalizeFirstLetter, getLangUrl } from '../config/utils';
import TopNavigationWelcome from './TopNavigationWelcome';

const TopNavigation = ({ data, openRegionPicker }) => {
  const router = useRouter();
  const [showWelcomeNav, setShowWelcomeNav] = useState(false);
  const lang = router.query.lang || Constants.defaultCountry.lang;

  useEffect(() => {
    if (openRegionPicker) {
      setShowWelcomeNav(openRegionPicker);
    }
  }, [openRegionPicker]);

  const getDefaultLangValue = languageUrlDictionary => {
    for (let key in languageUrlDictionary) {
      let url = languageUrlDictionary[key];
      if (url) {
        let path = getLangUrl(url);
        if (path && path.indexOf(lang) > -1) {
          return path;
        }
      }
    }
    return '';
  };

  const onLangChange = event => {
    let base_url = window.location.origin;
    window.location = base_url + event.target.value;
  };

  return (
    <>
      {data && (
        <div className="top-navigation--root top-navigation--show fadeIn cssanimation">
          <div className="container">
            {showWelcomeNav && (
              <TopNavigationWelcome onClose={setShowWelcomeNav} data={data} />
            )}

            <div
              className={`top-navigation--container ${
                !data.topNavigationMessage || data.topNavigationMessage === '\n'
                  ? 'tn-message'
                  : ''
              }`}
            >
              {data.topNavigationMessage && (
                <div
                  style={{ textAlign: 'center' }}
                  dangerouslySetInnerHTML={{
                    __html: data.topNavigationMessage
                  }}
                />
              )}
              <nav className="top-navigation--nav">
                {data.topNavigationSiteLinks &&
                  data.topNavigationSiteLinks.map(link => (
                    <a className="link--root" href={link.link} key={link.link}>
                      {link.htmlIcon && (
                        <div
                          style={{ textAlign: 'center', paddingBottom: '5px' }}
                          dangerouslySetInnerHTML={{ __html: link.htmlIcon }}
                        />
                      )}
                      {link.icon && (
                        <img src={link.icon} alt="" className="link--icon" />
                      )}

                      <span>{link.label}</span>
                    </a>
                  ))}

                {data && data.currentMarket && (
                  <>
                    <a
                      href="#"
                      onClick={() => setShowWelcomeNav(!showWelcomeNav)}
                      className="link--root arrow-link--root top-navigation--region"
                    >
                      <span className="region--country top-navigation">
                        <span
                          className={`flag-icon ${data.currentMarket.flag} region--country-flag`}
                        ></span>
                        <span>
                          <span className="region--country-title">
                            {data.currentMarket.marketName}
                            <span className="region--country-currency">
                              | {data.currentMarket.currencySymbol}
                            </span>
                          </span>
                        </span>
                      </span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 21.24 34"
                        className="arrow-link--icon"
                        focusable="false"
                        aria-hidden="true"
                      >
                        <path
                          d="M3,34a3,3,0,0,1-2.12-.88,3,3,0,0,1,0-4.24L12.76,17,.88,5.12A3,
                          3,0,0,1,5.12.88L21.24,17,5.12,33.12A3,3,0,0,1,3,34Z"
                        ></path>
                      </svg>
                    </a>
                    <span className="link--root arrow-link--root region--topnav-lang-area">
                      <select
                        className="region--topnav-lang-select"
                        aria-label="Select site language"
                        name="languageUrls"
                        onChange={onLangChange}
                        defaultValue={getDefaultLangValue(
                          data.currentMarket.languageUrlDictionary
                        )}
                      >
                        {data.currentMarket &&
                          Object.keys(
                            data.currentMarket.languageUrlDictionary
                          ).map(key => (
                            <option
                              key={key}
                              value={getLangUrl(
                                data.currentMarket.languageUrlDictionary[key]
                              )}
                            >
                              {capitalizeFirstLetter(key)}
                            </option>
                          ))}
                      </select>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 21.24 34"
                        className="arrow-link--icon"
                        focusable="false"
                        aria-hidden="true"
                      >
                        <path
                          d="M3,34a3,3,0,0,1-2.12-.88,3,3,0,0,1,0-4.24L12.76,17,.88,5.12A3,
                          3,0,0,1,5.12.88L21.24,17,5.12,33.12A3,3,0,0,1,3,34Z"
                        ></path>
                      </svg>
                    </span>
                    <a
                      className="link--root arrow-link--root"
                      href={data.accountPageUrl}
                    >
                      <span>My account</span>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 21.24 34"
                        className="arrow-link--icon"
                        focusable="false"
                        aria-hidden="true"
                      >
                        <path
                          d="M3,34a3,3,0,0,1-2.12-.88,3,3,0,0,1,0-4.24L12.76,17,.88,5.12A3,
                          3,0,0,1,5.12.88L21.24,17,5.12,33.12A3,3,0,0,1,3,34Z"
                        ></path>
                      </svg>
                    </a>{' '}
                  </>
                )}
              </nav>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default TopNavigation;
