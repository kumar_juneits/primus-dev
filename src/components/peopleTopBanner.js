import React from 'react';

import { useCountry } from '../providers/countryProvider';
import styles from '../styles/peopleTopBanner.module.css';
import BackButton from './backButton';
import PageBreadcrumb from './PageBreadcrumb';

const PeopleTopBanner = ({ people }) => {
  const { genUrl } = useCountry();
  const breadcrumbs = [
    { link: genUrl('/'), label: 'Home' },
    { link: genUrl('/people'), label: 'All people' },
    { label: people.firstname + ' ' + people.lastname }
  ];

  return (
    <div className="position-relative">
      <div className={styles.topBanner}>
        <img src={people.bannerImage} alt="" />
      </div>
      <div
        className={`position-absolute w-100 m-auto ${styles.breadcrumbContent}`}
      >
        <PageBreadcrumb
          breadcrumbs={breadcrumbs}
          className={'breadcrumbWhite'}
        />
        <BackButton href={genUrl(`people`)} />
      </div>
    </div>
  );
};

export default PeopleTopBanner;
