import { useRouter } from 'next/router';
import React from 'react';

import { Constants } from '../config/constants';
import { uriToLang } from '../config/utils';
import { useCountry } from '../providers/countryProvider';
import { useGlobal } from '../providers/globalProvider';
import Styles from '../styles/peopleCardStyle.module.css';
import { ChevronRight } from './icons';

const PeopleCard = ({ people }) => {
  const router = useRouter();
  const lang = uriToLang(router.query.lang || Constants.defaultCountry.lang);
  const countryCode = router.query.code || Constants.defaultCountry.code;
  const { siteSettings } = useGlobal();
  const { genUrl } = useCountry();

  return (
    <div key={people.id} className="col-sm-4 mb-4">
      <div className={Styles.FeatureCard}>
        <div className={Styles.cardBackgroundImage}>
          <a href={genUrl(`people/${people.id}`)}>
            <img src={people.imgURL} alt="" width="290px" height="164px" />
          </a>
        </div>
        <div className={Styles.cardAbout}>
          <div className={Styles.cardTitle}>
            <a href={genUrl(`people/${people.id}`)}>
              {people.firstname} {people.lastname}
            </a>
          </div>
          <div className={Styles.addresCard}>
            <div className="d-flex align-items-center">
              <div className={Styles.sustainability}>
                <span>{people.tag}</span>
              </div>
              <div>
                <img
                  src={`/assets/flags/${countryCode}.svg`}
                  className={Styles.flagImg}
                  width="24px"
                  alt=""
                />
              </div>
              <div className={Styles.location}>{people.country}</div>
            </div>
          </div>
          <div className={Styles.cardDescription}>
            {people.shortDescription}
          </div>
        </div>
        <a href={genUrl(`people/${people.id}`)} className={Styles.goToEvent}>
          {siteSettings?.terms[lang]?.getToKnow} {people.firstname}
          <div className={Styles.chevronRightIcon}>
            <ChevronRight />
          </div>
        </a>
      </div>
    </div>
  );
};

export default PeopleCard;
